const express = require("express");
const Vigenere = require('caesar-salad').Vigenere;

const app = express();
const port = 8080;

app.get('/encode/:data', (req, res) => {
  res.send(Vigenere.Cipher('data').crypt(req.params.data));
})
app.get('/decode/:data', (req, res) => {
  res.send(Vigenere.Decipher('data').crypt(req.params.data));
})
app.get('/:url', (req, res) => {
  res.send(req.params.url);
})
app.get('/', (req, res) => {
  res.send('Hello');
})

app.listen(port, () => {
  console.log('We are live ~~!!! ' + port);
})